﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game2.Music
{
    public class MusicManager
    {
        bool spacePressed = false;
        bool backPressed = false;

        bool started = false;

        SoundEffect songMenu;
        SoundEffect song1;
        SoundEffect song2;
        SoundEffect song3;
        SoundEffect song4;

        bool started1 = false;
        bool started2 = false;
        bool started3 = false;
        bool started4 = false;

        SoundEffectInstance instanceMenu;
        SoundEffectInstance instance1;
        SoundEffectInstance instance2;
        SoundEffectInstance instance3;
        SoundEffectInstance instance4;

        public void LoadContent()
        {
            FileStream streamMenu = File.Open(@"GameMusic/0 Menu Music_tail.wav", FileMode.Open);
            FileStream stream1 = File.Open(@"GameMusic/1 Level 1_tail.wav", FileMode.Open);
            FileStream stream2 = File.Open(@"GameMusic/2 Level 2_tail.wav", FileMode.Open);
            FileStream stream3 = File.Open(@"GameMusic/3 Level 3_tail.wav", FileMode.Open);
            FileStream stream4 = File.Open(@"GameMusic/4 Boss Level_tail.wav", FileMode.Open);

            songMenu = SoundEffect.FromStream(streamMenu); // Content.Load<Song>("song_title");  // Put the name of your song here instead of "song_title"
            song1 = SoundEffect.FromStream(stream1); // Content.Load<Song>("song_title");  // Put the name of your song here instead of "song_title"
            song2 = SoundEffect.FromStream(stream2); // Content.Load<Song>("song_title");  // Put the name of your song here instead of "song_title"
            song3 = SoundEffect.FromStream(stream3); // Content.Load<Song>("song_title");  // Put the name of your song here instead of "song_title"
            song4 = SoundEffect.FromStream(stream4);
        }

        public void Update(GameTime gameTime)
        {
            if (!started)
            {
                instanceMenu = songMenu.CreateInstance();
                instanceMenu.Volume = 1;
                instanceMenu.IsLooped = true;
                instanceMenu.Play();

                instance1 = song1.CreateInstance();
                instance1.Volume = 0;
                instance1.IsLooped = true;
                instance1.Play();

                instance2 = song2.CreateInstance();
                instance2.Volume = 0;
                instance2.IsLooped = true;
                instance2.Play();

                instance3 = song3.CreateInstance();
                instance3.Volume = 0;
                instance3.IsLooped = true;
                instance3.Play();

                instance4 = song4.CreateInstance();
                instance4.Volume = 0;
                instance4.IsLooped = true;
                instance4.Play();

                //MediaPlayer.Play(song1);
                //MediaPlayer.Play(song2);
                //MediaPlayer.Play(song3);

                //started1 = true;

                started = true;
            }

            if (!Game1.Instance.titleScreenManager.canStart)
            {
                return;
            }

            if (started1 == true)
            {
                if (instance1.Volume <= 1)
                {
                    instance1.Volume = MathHelper.Min(1, instance1.Volume + gameTime.ElapsedGameTime.Milliseconds / 4000f);
                }
            }
            else
            {
                if (instance1.Volume > 0)
                {
                    instance1.Volume = MathHelper.Max(0, instance1.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                }
            }

            if (started2 == true)
            {
                if (instance2.Volume <= 1)
                {
                    instance2.Volume = MathHelper.Min(1, instance2.Volume + gameTime.ElapsedGameTime.Milliseconds / 4000f);

                    if (instance1.Volume > 0)
                    {
                        instance1.Volume = MathHelper.Max(0, instance1.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                    }
                }
            }
            else
            {
                if (instance2.Volume > 0)
                {
                    instance2.Volume = MathHelper.Max(0, instance2.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                }
            }

            if (started3 == true)
            {
                if (instance3.Volume <= 1)
                {
                    instance3.Volume = MathHelper.Min(1, instance3.Volume + gameTime.ElapsedGameTime.Milliseconds / 4000f);

                    if (instance1.Volume > 0)
                    {
                        instance1.Volume = MathHelper.Max(0, instance1.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                    }

                    if (instance2.Volume > 0)
                    {
                        instance2.Volume = MathHelper.Max(0, instance2.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                    }
                }
            }
            else
            {
                if (instance3.Volume > 0)
                {
                    instance3.Volume = MathHelper.Max(0, instance3.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                }
            }

            if (started4 == true)
            {
                if (instance4.Volume <= 1)
                {
                    instance4.Volume = MathHelper.Min(1, instance4.Volume + gameTime.ElapsedGameTime.Milliseconds / 4000f);

                    if (instance1.Volume > 0)
                    {
                        instance1.Volume = MathHelper.Max(0, instance1.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                    }

                    if (instance2.Volume > 0)
                    {
                        instance2.Volume = MathHelper.Max(0, instance2.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                    }

                    if (instance3.Volume > 0)
                    {
                        instance3.Volume = MathHelper.Max(0, instance3.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                    }
                }
            }
            else
            {
                if (instance4.Volume > 0)
                {
                    instance4.Volume = MathHelper.Max(0, instance4.Volume - gameTime.ElapsedGameTime.Milliseconds / 4000f);
                }
            }

            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.Space) && !spacePressed)
            {
                spacePressed = true;

                if (!started1)
                {
                    Game1.Instance.titleScreenManager.closed = true;
                    Game1.Instance.LoadLevel();

                    started1 = true;
                    instanceMenu.Volume = 0;
                }
                else if (!started2)
                {
                    started2 = true;
                }
                else if (!started3)
                {
                    started3 = true;
                }
                else if (!started4)
                {
                    started4 = true;
                }
            }
            else if (!state.IsKeyDown(Keys.Space) && spacePressed)
            {
                spacePressed = false;
            }

            if (state.IsKeyDown(Keys.LeftAlt) && !backPressed)
            {
                backPressed = true;

                if (started4)
                {
                    started4 = false;
                }
                else if (started3)
                {
                    started3 = false;
                }
                else if (started2)
                {
                    started2 = false;
                }
                else if (started1)
                {
                    started1 = false;
                    instanceMenu.Volume = 1;
                }
            }
            else if (!state.IsKeyDown(Keys.LeftAlt) && backPressed)
            {
                backPressed = false;
            }
        }
    }
}
