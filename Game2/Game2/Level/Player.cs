﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game2.Level
{
    class Player
    {
        Texture2D playerTexture;
        public int X;
        public int Y;

        SpriteBatch spriteBatch;

        const int textureWidth = 32;
        const int textureHeight = 32;


        public Player()
        {

        }

        public void LoadContent(GraphicsDevice graphicsDevice)
        {
            spriteBatch = new SpriteBatch(graphicsDevice);

            FileStream fileStream = File.OpenRead("Images/Player.png");
            playerTexture = Texture2D.FromStream(graphicsDevice, fileStream);

        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            double centerX = Game1.Instance.Window.ClientBounds.Width / 2d;
            double centerY = Game1.Instance.Window.ClientBounds.Height / 2d;

            spriteBatch.Draw(playerTexture, new Rectangle((int)(centerX - (textureWidth / 2d)), (int)(centerY - (textureHeight / 2d)), textureWidth, textureHeight), Color.White);
        
            spriteBatch.End();
        }
    }
}
