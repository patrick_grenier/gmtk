﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game2.Level
{
    class LevelManager
    {
        Texture2D levelBitmap;
        Color[] levelColorData;
        String levelName;

        Player player;

        public LevelManager(String levelName)
        {
            this.levelName = levelName;
            player = new Player();
            LoadContent(Game1.Instance.GraphicsDevice);

            
        }

        public void LoadContent(GraphicsDevice graphicsDevice)
        {
            FileStream fileStream = File.OpenRead("LevelData/" + levelName + ".bmp");
            levelBitmap = Texture2D.FromStream(graphicsDevice, fileStream);

            Color[] levelColorData1D = new Color[levelBitmap.Width * levelBitmap.Height];
            levelBitmap.GetData(levelColorData1D);

            Color[,] levelColorData = new Color[levelBitmap.Width, levelBitmap.Height];
            for (int x = 0; x < levelBitmap.Width; x++)
            {
                for (int y = 0; y < levelBitmap.Height; y++)
                {
                    levelColorData[x, y] = levelColorData1D[x + y * levelBitmap.Width];
                }
            }

            player.LoadContent(graphicsDevice);
        }

        public void Update(GameTime gameTime)
        {
            player.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            player.Draw(gameTime);
        }
    }
}
