﻿using Game2.Level;
using Game2.Music;
using Game2.TitleScreen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.IO;

namespace Game2
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public static Game1 Instance;

        public MusicManager musicManager;
        public TitleScreenManager titleScreenManager;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        LevelManager currentLevel;
        
        // Used for 3D rendering
        //private Matrix world = Matrix.CreateTranslation(new Vector3(0, 0, 0));
        //private Matrix view = Matrix.CreateLookAt(new Vector3(0, 0, 10), new Vector3(0, 0, 0), Vector3.UnitY);
        //private Matrix projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 800f / 480f, 0.1f, 100f);

        public Game1()
        {
            Instance = this;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            musicManager = new MusicManager();
            titleScreenManager = new TitleScreenManager();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            string ContentFullPath = Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), Content.RootDirectory);


            //model = Content.Load<Model>(Path.Combine(ContentFullPath, "GMT_SCENE_1.fbx"));

            //model = Content.Load<Model>("C://Users/patrick.grenier/Desktop/GameJam/GMT_SCENE_1.fbx");

            musicManager.LoadContent();
            titleScreenManager.LoadContent(GraphicsDevice);
            
            // TODO: use this.Content to load your game content here
            //background = Content.Load<Texture2D>("background");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        ///
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            titleScreenManager.Update(gameTime);

            musicManager.Update(gameTime);

            if (currentLevel != null)
            {
                currentLevel.Update(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            titleScreenManager.Draw(gameTime);
            
            if (currentLevel != null)
            {
                currentLevel.Draw(gameTime);
            }


            base.Draw(gameTime);
        }

        public void LoadLevel()
        {
            currentLevel = new LevelManager("Level1");


        }

    }
}
