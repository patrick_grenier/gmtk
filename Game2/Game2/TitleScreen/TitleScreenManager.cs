﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game2.TitleScreen
{
    public class TitleScreenManager
    {
        Texture2D gmtkImage;
        float gmtkAlpha = 1f;

        Texture2D titleImage;
        float titleAlpha = 0f;

        SpriteBatch spriteBatch;

        private SpriteFont font;

        public bool canStart = false;
        public bool closed = false;

        public void LoadContent(GraphicsDevice graphicsDevice)
        {
            font = Game1.Instance.Content.Load<SpriteFont>("title");

            spriteBatch = new SpriteBatch(graphicsDevice);

            FileStream fileStream = File.OpenRead("TitleScreen/mgtk.png");
            gmtkImage = Texture2D.FromStream(graphicsDevice, fileStream);

            FileStream fileStreamTitle = File.OpenRead("TitleScreen/title.jpg");
            titleImage = Texture2D.FromStream(graphicsDevice, fileStreamTitle);
        }

        public void Draw(GameTime gameTime)
        {
            if (closed)
            {
                return;
            }

            spriteBatch.Begin();

            if (gmtkAlpha > 0)
            {
                spriteBatch.Draw(gmtkImage, new Rectangle(0, 0, Game1.Instance.Window.ClientBounds.Width, Game1.Instance.Window.ClientBounds.Height), Color.White * gmtkAlpha);
            }
            else
            {
                spriteBatch.Draw(titleImage, new Rectangle(0, 0, Game1.Instance.Window.ClientBounds.Width, Game1.Instance.Window.ClientBounds.Height), Color.White * titleAlpha);

                if (titleAlpha >= 1)
                {
                    if (gameTime.TotalGameTime.TotalMilliseconds % 1000 > 500)
                    {
                        canStart = true;
                        spriteBatch.DrawString(font, "Press Space", new Vector2(Game1.Instance.Window.ClientBounds.Width / 2f - font.MeasureString("Press Space").X / 2f, Game1.Instance.Window.ClientBounds.Height / 2f - font.MeasureString("Press Space").Y / 2f), Color.White);
                    }
                }
            }

            spriteBatch.End();
        }


        public void Update(GameTime gameTime)
        {
            if (closed)
            {
                return;
            }

            if (gameTime.TotalGameTime.TotalSeconds > 2 && gmtkAlpha > 0)
            {
                gmtkAlpha = Math.Max(0, gmtkAlpha - gameTime.ElapsedGameTime.Milliseconds / 1000f * 2f); // fade out for half second
            }

            if (gmtkAlpha <= 0 && titleAlpha < 1)
            {
                titleAlpha = Math.Min(1, titleAlpha + gameTime.ElapsedGameTime.Milliseconds / 1000f / 2f); // fade in for 2 seconds
            }
        }
    }
}
